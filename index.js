/**
 * Name: Yash Khanchandani
 * Date: June 30, 2020
 * Section: CSE 154 AC
 * This is the JS to implement the UI for my cryptogram generator, and generate
 * different types of ciphers from user input.
 */
/**
 * Global Varibles
 */
"use strict";
const checkListInput = document.querySelector(".check-list-input");
const checkListButton = document.querySelector(".check-list-button");
const checkListList = document.querySelector(".check-list-list");

checkListButton.addEventListener("click", addBox);
checkListList.addEventListener('click', deleteCheck);

/**
 * Add the function to add the box of the checklist
 * @param {object} event
 */

function addBox(event) {
  event.preventDefault();

  const checkListDiv = document.createElement("div");
  checkListDiv.classList.add("box");

  const newCheckList = document.createElement('li');
  newCheckList.innerText = checkListInput.value;
  newCheckList.classList.add('check-list-item');
  checkListDiv.appendChild(newCheckList);

  const checkButton = document.createElement('button');
  checkButton.innerText = 'Check';
  checkButton.classList.add("checkButt");
  checkListDiv.appendChild(checkButton);

  const deleteButton = document.createElement('button');
  deleteButton.innerText = 'Delete';
  deleteButton.classList.add("deleteButt");
  checkListDiv.appendChild(deleteButton);

  checkListList.appendChild(checkListDiv);

  checkListInput.value = "";
}

/**
 * Add the cability of marking the task done or deleting the task.
 * @param {object} evnt
 */

function deleteCheck(evnt) {
  const item = evnt.target;
  if (item.classList[0] === 'deleteButt') {
    const box = item.parentElement;
    box.remove();
  }

  if (item.classList[0] === 'checkButt') {
    const box = item.parentElement;
    box.classList.toggle("checkMark");
  }
}